from django.db import models

# Create your models here.

class Servicio (models.Model):

    id_servicio = models.IntegerField(primary_key=True, verbose_name="ID Servicio")

    nombre_servicio = models.CharField(max_length=50, verbose_name="Servicio")


    def __str__(self):

        return self.nombre_servicio

class Proveedor (models.Model):

    rut = models.IntegerField(primary_key=True, verbose_name="Rut proveedor")

    nombre = models.CharField(max_length=50, verbose_name="Nombre")

    descripcion = models.TextField(max_length=100,verbose_name="Descripcion")

    email = models.CharField(max_length=50, verbose_name="Email")

    telefono = models.CharField(max_length=20, verbose_name="Telefono")

    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)

    def __str__(self):

        return self.nombre

class Categoria (models.Model):
    nombre = models.CharField(max_length=100)
    activa = models.BooleanField(default=True)

    def __str__(self):

        return self.nombre


class Producto (models.Model):
    sku = models.IntegerField(primary_key=True, verbose_name="sku" )

    nombre = models.CharField(max_length=50, verbose_name="Nombre")

    precio = models.IntegerField(verbose_name="Precio")

    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)

    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE)

    imagen_url =  models.CharField(max_length=1000, verbose_name="imagen URL", null=True, blank=True)

    def __str__(self):

        return self.nombre


    


