from django.urls import path
from .views import index, \
 sesion, registrate, registrar, contacto, \
 listar_proveedores, modificar_proveedor, nuevo_proveedor, eliminar_proveedor, \
 listar_productos, nuevo_producto, modificar_producto, eliminar_producto

urlpatterns = [
    path('', index, name="index"),
    path('index/', index, name="index"),
    path('contacto/', contacto, name="contacto"),
    
    # Registro
    path('registrate/', registrate, name="registrate"),
    path('registrar/', registrar, name='registrar'),
    path('sesion/', sesion, name="sesion"),

    # Provedores
    path('listar-proveedores/', listar_proveedores, name="listar_proveedores"),
    path('modificar-proveedor/<id>', modificar_proveedor, name='modificar_proveedor'),
    path('nuevo-proveedor/', nuevo_proveedor, name='nuevo_proveedor'),
    path('eliminar-proveedor/<id>', eliminar_proveedor, name='eliminar_proveedor'),

    # Productos
    path('listar-productos/', listar_productos, name="listar_productos"),
    path('modificar-producto/<id>', modificar_producto, name='modificar_producto'),
    path('nuevo-producto/', nuevo_producto, name='nuevo_producto'),
    path('eliminar-producto/<id>', eliminar_producto, name='eliminar_producto'),
]

