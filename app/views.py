from django.shortcuts import render, redirect, get_object_or_404
from .models import Proveedor, Producto
from .forms import ProveedorForm, CustomUserCreationForm, ProductoForm
from django.contrib import messages
from django.contrib.auth import authenticate, login

# Create your views here.

# index
def index(request):
    productos = Producto.objects.all()

    contexto = {
        'productos' : productos
    }
    return render(request, 'app/index.html', contexto)

# Proveedor

def listar_proveedores(request):
    proveedores = Proveedor.objects.all()

    contexto = {
        'proveedores' : proveedores
    }
    return render(request, 'app/listar_proveedores.html', contexto)

def nuevo_proveedor (request):
    data = {
        'form' : ProveedorForm()
    }
    if request.method=='POST':
        formulario = ProveedorForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request,"Agregado correctamente")
            return redirect(to="/listar-proveedores/")
        else:
            data["form"] = formulario
    return render(request, 'app/nuevo_proveedor.html', data)

def modificar_proveedor (request,id):
    proveedor =get_object_or_404(Proveedor,rut = id)
    datos = {
        'form' : ProveedorForm(instance=proveedor)
    }
    if request.method=='POST':
        formulario = ProveedorForm(data=request.POST,instance=proveedor,files=request.FILES)
        if formulario.is_valid:
            formulario.save()
            messages.success(request,"Modificado correctamente")
            return redirect(to="/listar-proveedores/")
        else:
           datos["form"] = formulario
    return render(request, 'app/modificar_proveedor.html', datos)

def eliminar_proveedor(request,id):
    proveedor = get_object_or_404(Proveedor,rut = id)
    proveedor.delete()
    messages.success(request,"Eliminado correctamente")
    return redirect(to="listar_proveedores")

# Registro
def registrar(request):

    data= {
        'form': CustomUserCreationForm()
    }

    if request.method == 'POST':
        formulario = CustomUserCreationForm(data = request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"],password=formulario.cleaned_data["password1"])
            messages.success(request,"Enviado Correctamente")
            login(request,user)
            return redirect(to="index")
            
        data["form"] = formulario

    return render(request,'registration/registrate.html',data)

def sesion(request):
    return render(request, 'app/sesion.html')

def registrate(request):
    return render(request, 'app/registrate.html')

# Contactanos
def contacto(request):
    return render(request, 'app/contacto.html')

# PRODUCTOS
def listar_productos(request):
    productos = Producto.objects.all()

    contexto = {
        'productos' : productos
    }
    return render(request, 'app/listar_productos.html', contexto)



def nuevo_producto (request):
    data = {
        'form' : ProductoForm()
    }
    if request.method=='POST':
        formulario = ProductoForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request,"Agregado correctamente")
            return redirect(to="/listar-productos/")
        else:
            data["form"] = formulario
    return render(request, 'app/nuevo_producto.html', data)

def modificar_producto (request, id):
    producto = get_object_or_404(Producto,sku = id)
    datos = {
        'form' : ProductoForm(instance=producto)
    }
    if request.method=='POST':
        formulario = ProductoForm(data=request.POST,instance=producto,files=request.FILES)
        if formulario.is_valid:
            formulario.save()
            messages.success(request,"Modificado correctamente")
            return redirect(to="/listar-productos/")
        else:
           datos["form"] = formulario
    return render(request, 'app/modificar_producto.html', datos)
    
def eliminar_producto(request, id):
    producto = get_object_or_404(Producto, sku = id)
    producto.delete()
    messages.success(request,"Eliminado correctamente")
    return redirect(to="listar_productos")