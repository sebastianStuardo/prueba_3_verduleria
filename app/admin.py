from django.contrib import admin
from .models import Proveedor, Servicio, Producto, Categoria

# Register your models here.

admin.site.register(Proveedor)
admin.site.register(Servicio)
admin.site.register(Producto)
admin.site.register(Categoria)

